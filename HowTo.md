# Definition of benchmark jobs

/afs/cern.ch/atlas/project/pmb/new_pmb/bin/pmb_cron.sh

calls 

/afs/cern.ch/atlas/project/pmb/new_pmb/bin/pmb_cron.py

and ensures not more than 8 athena jobs can run at any time

pmb_cron.sh is called every half hour on *aibuild010.cern.ch* as called using acron 

To open acron with emacs
```
acrontab -e

0,30 * * * * aibuild010.cern.ch /afs/cern.ch/atlas/project/pmb/new_pmb/bin/pmb_cron.sh  >/dev/null 2>&1         


```
*aibuild010.cern.ch* is a machine solely used by atlaspmb to run benchmark jobs. The environment is purposefully quiet in order to maintain consistency of cpu time measurements


pmb_cron.py creates jobs and lauches them. The jobs to be run are defined in the variable

```
joblist_default = [
    fullchain_mc15_ttbar_valid_13tev_25ns_mu40,
    rawtoall_tier0_reco_data16,
    system_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40,
    combined_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40,
    ]

```
where the list items are python functions, which call other functions to fully specify the job to run and the needed input files

Naturally these jobs must be run within a given Athena build, as such the release and platform should be chosen

```
    runlist+=[('21.0','x86_64-slc6-gcc62-opt',joblist_default)]
    runlist+=[('master','x86_64-slc6-gcc62-opt',joblist_default)]
    
```
Above releeases 21.0 and masters with build platform x86_64-slc6-gcc62-opt are chosen

As the PMB guru you'll most often just update the runlist variable to keep up with the latest numbered releases. Sometime you'll define a new job to add to the joblist_default list.

Let's go through an example of defining a new job. Our example will be simulation.

Define a function for the athena executable
```
def Sim_tf_cmd():
    return 'Sim_tf.py'
```

Define the arguments and explicitly specify the inputfile flag in a function

```
def __Sim_tf_evnttohits(infile,nevts,short,amp):
    if short: nevts=1
    f=infile
    arguments=' --conditionsTag \'default:OFLCOND-RUN12-SDR-19\' --physicsList \'FTFP_BERT\' --truthStrategy \'MC15aPlus\' --simulator \'FullG4\' --postInclude \'default:RecJobTransforms/UseFrontier.py\' --preInclude \'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py\' --DataRunNumber \'222525\' --geometryVersion \'default:ATLAS-R2-2015-03-01-00_VALIDATION\' --inputEVNTFile "/afs/cern.ch/atlas/groups/Simulation/EVNT_files/mc12_valid.110401.PowhegPythia_P2012_ttbar_nonallhad.evgen.EVNT.e3099.01517252._000001.pool.root.1" --outputHITSFile "Hits.pool.root" --maxEvents 20 --preExec \'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;\' --ignoreErrors \'True\' '+amp+' '
    return (infile,f),'%s --inputEVNTFile %s --maxEvents %i %s'%(Sim_tf_cmd(),f,nevts,arguments)
```

Define the function that calls the above function and specifies the input file
```
def __evnttohits(branch,cmtcfg,builddate,short,nevts):
    infile='/build/atlaspmb/mc/mc12_valid.110401.PowhegPythia_P2012_ttbar_nonallhad.evgen.EVNT.e3099.01517252._000001.pool.root.1'
    return __Sim_tf_evnttohits(infile,__nevts(nevts,builddate),short)
```

Finally define the job function
```
def fullsim_mc15a(**kw): return __fullsim_mc15a(nevts=20,**kw)
```

which can be added to a new joblist (or the one already defined) and to the runlist

```
joblist_simulation=['fullsim_mc15a']
runlist+=[('21.3','x86_64-slc6-gcc62-opt',joblist_simulation)]

````

So in all four functions were defined and two lists created or made, for a new benchmark job to be defined.

The jobs are launched in and run from the /build/atlaspmb/custom_nightly_tests/rundirs and the job outputs are then copied into the eos archive area under
```
/eos/atlas/user/a/atlaspmb/archive/custom/<day>/<month>/<year>/<release>/<platform>/<jobname>/
```

pmb_cron.py creates a *__command.sh* executable which runs the job and does the post processing of creating a sqlite database file with all relevant performance data

the *cmd_gen* function is the engine for producing __command.sh

# Creating a web page

Creating a webpage requires editing the master.sh file in the scripts directory. 




