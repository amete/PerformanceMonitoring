AtlasPMB is responsible for maintaining the RunTier0Tests.py script which is located here

https://gitlab.cern.ch/atlas/athena/blob/master/Tools/PROCTools/python/RunTier0Tests.py

In addition to scheduling q-tests q221 and q431 with/out intended changes, it tests for  changes in the outputs (Frozen Tier0 Policy), cpu time, memory consumption, and memory leak parameter. 

Issue 

https://its.cern.ch/jira/browse/ATEAM-195

