### Profiling memory

Profiling of heap memory is described in detail here

https://twiki.cern.ch/twiki/bin/view/ITSDC/ProfAndOptExperimentsApps#Heap_Profiling

As stated on this page, heap profiling can be performed on ATLAS jobs using the "jemalloc" memory allocator, which was developed by Facebook.



Or using Igprof

http://igprof.org/

https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/PerfMonSD
https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/OptimisingCode#Profiling_Athena_with_gathena
https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/MonitoringMemory


Testing of different memory allocators is being followed in this issue

https://its.cern.ch/jira/browse/ATEAM-207




