Web pages are hosted at

/eos/project/a/atlasweb/www/atlas-pmb

Job name directories begin with "arch-mon" e.g.

arch-mon-rawtoall_tier0_reco_data16

and so the naming convention is arch-mon-<jobname>

The master.sh script will automatically copy the web directory including all plots into this area.

But when making a new webpage you must copy files named

index.php  menustyle.css

into the arch-mon-<jobname> directory

