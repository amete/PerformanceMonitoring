#!/bin/bash

source ~/.bashrc
source ~/.bash_profile
lsetup "lcgenv -p LCG_95 x86_64-centos7-gcc8-opt pytools"
eosfusebind -g # ATLINFR-3256

export PYTHONPATH=/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring/python:$PYTHONPATH

# First bit of Python
python << END
from os import path
from sqlalchemy import *
import pmbDB as mydb
import glob
import time
import os

x=mydb.pmbDB('/build/atlaspmb/custom_nightly_tests/database_staging_area/master_7.db')

filelist=glob.glob('/build/atlaspmb/custom_nightly_tests/database_staging_area/__b*db')

current_time = time.time()

for file in filelist[:]:
    creation_time = path.getctime(file)
    if (current_time - creation_time) > 3600000 :     
        filelist.remove(file)
    
x.merge_db_files(filelist)    


for file in filelist:
    os.remove(file)
END

# Once the database is merged, copy it
rsync -auvz /build/atlaspmb/custom_nightly_tests/database_staging_area/master_7.db /eos/atlas/user/a/atlaspmb/archive/custom/.

# Here comes the second bit of Python
python << END
from os import path
from sqlalchemy import *
import pmbDB as mydb
import pmbWebPage as myweb
import glob
import time
import sys
import os
import subprocess
import multiprocessing

# Job Config
dbName    ='/build/atlaspmb/custom_nightly_tests/database_staging_area/master_7.db'
releases  =['21.0','master'] # Only 21.2 used below for derivation jobs...
pathname  ='/build4/atlaspmb/www_staging_area/'
platforms =['x86_64-slc6-gcc62-opt','x86_64-centos7-gcc8-opt']
#platforms =['x86_64-slc6-gcc62-opt','x86_64-centos7-clang9-opt','x86_64-centos7-gcc8-opt']
jobs      =['rawtoall_tier0_reco_data16', \
            'rawtoall_tier0_reco_data17', \
            'rawtoall_tier0_reco_data18', \
            'fullchain_mc15_ttbar_valid_13tev_25ns_mu40', \
            'fullchain_mc16_ttbar_valid_13tev_25ns_mu40', \
            'simulation_ttbar', \
            'fast_simulation_ttbar', \
            'derivation_physval_mc16', \
            'derivation_physval_data18', \
            'rdotordotrigger_q221_mt1', \
            'simulation_itk', \
            'rawtoall_mc_itk', \
            'digireco_mc_phase2_upgrade_mu60', \
            'digireco_mc_phase2_upgrade_mu200', \
            'rawtoall_tier0_reco_data18_largeRTrack', \
            'fullchain_mc16_ttbar_valid_13tev_25ns_mu40_largeRTrack', ]
ndays     = 21

# List of processes
my_processes = {}

# Functions
def copy_outputs(jobname):
    cmd = 'rsync -avzu  '+pathname+'/arch-mon-'+jobname+' /eos/project/a/atlasweb/www/atlas-pmb/. '
    output,error = subprocess.Popen(['/bin/bash', '-c', cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    if not error:
        print("\nCopy job for {} is ALL OK!\n".format(jobname))
    else:
        print("\nCopy job for {} returned an ERROR!\n{}".format(jobname,error))

def my_worker_function(*args):
    # Get the variables
    jobname      = args[0]
    releasenames = args[1]
    platformnames= args[2]

    # Create the webpage class
    current_job=myweb.PMBWebPage(dbName,jobname,releasenames,platformnames,ndays,pathname)

    # Check the steps
    if current_job.steps == [] : 
       print jobname, " has no steps to process" 
       return 
    else:
       print "processing ",jobname

    current_job.Build_All_WebPages()

    # Comparison jobs
    if len(releasenames)>1 or len(platformnames)>1:
        for rel in releasenames:
            for plat in platformnames:
                comparison_job=myweb.PMBWebPage(dbName,jobname,[rel],[plat],ndays,pathname)
                comparison_job.Build_All_WebPages()

    # Copy the outputs
    copy_outputs(jobname)

# Limit the number of concurrent jobs to 10 at a time
chunk_size = 10
n_full = len(jobs)//chunk_size
n_offset = 1 if len(jobs)%chunk_size != 0 else 0

for idx in range(0,n_full+n_offset):
    n_min = idx*chunk_size
    n_max = (idx+1)*chunk_size if idx != n_full else len(jobs)

    print('Currently processing {}'.format(jobs[n_min:n_max]))

    # Add to processes
    my_processes = {}
    for job in jobs[n_min:n_max]:
        if 'derivation' in job:
            my_processes[job] = multiprocessing.Process(target=my_worker_function,args=(job,['21.2'],['x86_64-slc6-gcc62-opt']))
        elif 'trigger' in job or 'largeRTrack' in job:
            my_processes[job] = multiprocessing.Process(target=my_worker_function,args=(job,['master'],['x86_64-centos7-gcc8-opt']))
        elif 'itk' in job or 'upgrade' in job:
            my_processes[job] = multiprocessing.Process(target=my_worker_function,args=(job,['21.9'],['x86_64-slc6-gcc62-opt']))
        else:
            my_processes[job] = multiprocessing.Process(target=my_worker_function,args=(job,releases,platforms))
        my_processes[job].start()

    # Join to wait each process to finish its magic
    for process in my_processes:
        my_processes[process].join()

END

echo "Finished running master.sh, please check the outputs and the webpage." | mail -s "[ATLASPMB] master.sh cron job finished" atlaspmb@cern.ch
