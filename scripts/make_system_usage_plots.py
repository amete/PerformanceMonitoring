import sys
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.dates import DateFormatter, DayLocator, HourLocator, MinuteLocator
plt.style.use('seaborn-whitegrid')
from datetime import timedelta

# Define variables
nCPUs = { 'uclhc' : 40 , 'atpc002' : 32 , 'aibuild001' : 32 , 'gpatlas2' : 24 }
def getvariables(hostname):
    if hostname == 'uclhc-1.ps.uci.edu':
        node_short  = 'uclhc'
        workdir     = '/home/amete/system_monitoring/'
        foldernames  = ['/data']
    elif hostname == 'atpc002':
        node_short  = 'atpc002'
        workdir     = '/atlas/scratch0/amete/system_monitoring'
        foldernames  = ['/atlas/scratch0']
    elif hostname == 'aibuild001.cern.ch':
        node_short  = 'aibuild001'
        workdir     = '/afs/cern.ch/user/a/atlaspmb/system_monitoring'
        foldernames  = ['/build1','/build2','/build3','/build4']
    elif hostname == 'gpatlas2.local':
        node_short  = 'gpatlas2'
        workdir     = '/home/amete/system_monitoring/'
        foldername  = ['/data7/atlas']
    return (workdir,node_short,foldernames)

# Make CPU/IO load averages plot
def make_load_plot(workdir,node_short):
    # Load the averages data
    load_averages = pd.read_csv('%s/sys-logs/average-load-history.log'%(workdir), sep = ' ', header=None)
    load_averages[0] = pd.to_datetime(load_averages[0], unit='s')
    first_date_to_filter = load_averages[0][load_averages.shape[0]-1] - timedelta(days=2) # Keep the last two days 
    load_averages = load_averages[ load_averages[0] >= first_date_to_filter ]
    load_averages[6].replace({'powersave':0, 'performance':1}, inplace=True)

    # Make a simple 1-D plot
    fig, ax1 = plt.subplots()
    plt.plot(load_averages[0], load_averages[1], color='fuchsia', lw=2, label='Last 1 min.' )
    plt.plot(load_averages[0], load_averages[2], color='turquoise', lw=2, label='Last 5 min.' )
    plt.plot(load_averages[0], load_averages[3], color='cornflowerblue', lw=2, label='Last 15 min.')
    plt.title('Load Average (state R and D) on the %s'%(node_short.upper()))
    plt.xlabel('Date')
    plt.ylabel('Load Average')
    plt.legend(loc=2)
    ax1.axhline(y=nCPUs[node_short]*0.75, color = 'orange', lw=4, ls='--')
    ax1.axhline(y=nCPUs[node_short], color = 'red', lw=4, ls='--')
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(5))
    ax1.xaxis.set_major_locator(DayLocator())
    ax1.xaxis.set_major_formatter(DateFormatter('%d/%m/%y'))
    ax1.xaxis.set_minor_locator(HourLocator(byhour=range(2,24,2)))
    ax1.xaxis.set_minor_formatter(DateFormatter('%H'))
    #ax1.tick_params(labeltop=False, labelright=True)
    plt.setp(ax1.xaxis.get_majorticklabels(), rotation=90)
    plt.setp(ax1.xaxis.get_minorticklabels(), rotation=90)
    #ax1.tick_params(labeltop=False, labelright=True)
    plt.setp(ax1.xaxis.get_majorticklabels(), rotation=90)
    plt.setp(ax1.xaxis.get_minorticklabels(), rotation=90)
    plt.ylim(ymax=max([nCPUs[node_short],load_averages[1].max(),load_averages[2].max(),load_averages[3].max()])*1.35)
    fig.subplots_adjust(bottom=0.2)
    ax1.grid(which='minor', alpha=0.5)
    # Right axis
    ax2 = ax1.twinx()
    ax2.plot(load_averages[0], load_averages[6], color='green')
    ax2.xaxis.set_major_locator(DayLocator())
    ax2.xaxis.set_major_formatter(DateFormatter('%d/%m/%y'))
    ax2.xaxis.set_minor_locator(HourLocator(byhour=range(2,24,2)))
    ax2.xaxis.set_minor_formatter(DateFormatter('%H'))
    ax2.yaxis.set_major_locator(ticker.MultipleLocator(1))
    ax2.tick_params(axis='y', labelcolor='green')
    ax2.set_ylabel('CPU Scaling', color='green', rotation=90)
    ax2.set_ylim(ymin=-0.05,ymax=1.1)
    #ax2.set_yticks(np.linspace(ax2.get_yticks()[0], ax2.get_yticks()[-1], len(ax1.get_yticks()))) # to aling but not necessary
    ax2.grid(None)
    fig.savefig('%s-average-load.png'%(node_short))

# Make memory plot
# a la htop : https://stackoverflow.com/questions/41224738/how-to-calculate-system-memory-usage-from-proc-meminfo-like-htop
# Total used memory = MemTotal - MemFree
# Non cache/buffer memory (green) = Total used memory - (Buffers + Cached memory)
# Buffers (blue) = Buffers
# Cached memory (yellow) = Cached + SReclaimable - Shmem
# Swap = SwapTotal - SwapFree
def make_mem_plot(workdir,node_short):
    # MemTotal: MemFree: Buffers: Cached: SwapCached: SwapTotal: SwapFree: Shmem: SReclaimable: 
    # 1         2        3        4       5           6          7         8      9

    # Load the meminfo data
    mem_info = pd.read_csv('%s/sys-logs/memory-history.log'%(workdir), sep = ' ', header=None)
    mem_info[0] = pd.to_datetime(mem_info[0], unit='s')
    first_date_to_filter = mem_info[0][mem_info.shape[0]-1] - timedelta(days=2) # Keep the last two days
    mem_info = mem_info[ mem_info[0] >= first_date_to_filter ]

    # Measurements in line w/ htop
    mem_info = mem_info.assign(swap    = mem_info[6]-mem_info[7]            ) # Swap
    mem_info = mem_info.assign(cached  = mem_info[4]+mem_info[9]-mem_info[8]) # Cached memory (yellow)
    mem_info = mem_info.assign(buffers = mem_info[3]                        ) # Buffers (blue)
    mem_info = mem_info.assign(noncachedbuffermem=mem_info[1]-mem_info[2]-mem_info['buffers']-mem_info['cached']) # Non cache/buffer memory (green)  

    # Make a simple 1-D plot
    scale = 1./(1024.*1024.) # kB => GB
    fig, ax1 = plt.subplots()
    ax1.stackplot(mem_info[0].values,
                  mem_info['noncachedbuffermem'].values*scale,
                  mem_info['buffers'].values*scale,
                  mem_info['cached'].values*scale,
                  colors = ['green','blue','orange'],
                  labels = ['Non Cache/Buffer','Buffers','Cached'],
                  alpha  = 0.7)
    plt.title('Memory usage on the %s'%(node_short.upper()))
    plt.xlabel('Date')
    plt.ylabel('Memory [GB]')
    plt.legend(loc=2)
    ax1.axhline(y=mem_info[1].iloc[-1]*scale, color = 'red', lw=4, ls='--')
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(10))
    ax1.xaxis.set_major_locator(DayLocator())
    ax1.xaxis.set_major_formatter(DateFormatter('%d/%m/%y'))
    ax1.xaxis.set_minor_locator(HourLocator(byhour=range(2,24,2)))
    ax1.xaxis.set_minor_formatter(DateFormatter('%H'))
    ax1.tick_params(labeltop=False, labelright=True)
    plt.setp(ax1.xaxis.get_majorticklabels(), rotation=90)
    plt.setp(ax1.xaxis.get_minorticklabels(), rotation=90)
    plt.ylim(ymax=mem_info[1].iloc[-1]*scale*1.35)
    fig.subplots_adjust(bottom=0.2)
    ax1.grid(which='minor', alpha=0.5)
    fig.savefig('%s-mem-info.png'%(node_short))

# Make disk usage plot
def make_disk_plot(workdir,node_short,foldernames):
    # Load the disk 
    disk_info = pd.read_csv('%s/sys-logs/disk-history.log'%(workdir), sep = ' ', header=None)
    disk_info[0] = pd.to_datetime(disk_info[0], unit='s')
    first_date_to_filter = disk_info[0][disk_info.shape[0]-1] - timedelta(days=2) # Keep the last two days
    disk_info = disk_info[ disk_info[0] >= first_date_to_filter ]

    # Make a simple 1-D plot
    fig, ax1 = plt.subplots()
    colors = ['blue','green','purple','black']
    for idx,folder in enumerate(foldernames):
        plt.plot(disk_info[0], disk_info[idx+1], color=colors[idx], lw=2, label='%s'%(folder))
    plt.title('Disk Usage on the %s'%(node_short.upper()))
    plt.xlabel('Date')
    plt.ylabel('Usage [%]')
    plt.legend(loc=2, ncol=len(foldernames))
    ax1.axhline(y=70, color = 'orange', lw=4, ls='--')
    ax1.axhline(y=100, color = 'red', lw=4, ls='--')
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(10))
    ax1.xaxis.set_major_locator(DayLocator())
    ax1.xaxis.set_major_formatter(DateFormatter('%d/%m/%y'))
    ax1.xaxis.set_minor_locator(HourLocator(byhour=range(2,24,2)))
    ax1.xaxis.set_minor_formatter(DateFormatter('%H'))
    ax1.tick_params(labeltop=False, labelright=True)
    plt.setp(ax1.xaxis.get_majorticklabels(), rotation=90)
    plt.setp(ax1.xaxis.get_minorticklabels(), rotation=90)
    plt.ylim(ymin=0.,ymax=120.)
    fig.subplots_adjust(bottom=0.2)
    ax1.grid(which='minor', alpha=0.5)
    fig.savefig('%s-disk-info.png'%(node_short))

# Make plots
if '__main__' in __name__:
    workdir,node_short,foldernames = getvariables(sys.argv[1])
    make_load_plot(workdir,node_short)
    make_mem_plot(workdir,node_short)
    make_disk_plot(workdir,node_short,foldernames)
