#!/usr/bin/

# Script for running a few select jobs on a few select branches on
# specific build machine via cron. This mainly to be able to monitor
# developments in cpu usage.
#
# Several efforts are made to ensure that results are as reliable as
# possible: Job is only started if machine is not too busy, input files
# are copied locally before start, and each job is preceded by a small
# nevts=1 test job, to ensure all afs caches are set up.

import os,glob,time,datetime,sys,subprocess,shutil,subprocess,re

##############################################################################
############################# DEFINE DIRS ####################################
##############################################################################
cvmfs=True

username=os.getenv('USER')

if not os.getenv('BASEDIR'):
    print 'ERROR: $BASEDIR not defines'
    sys.exit(1)

archive_dir = '/eos/atlas/user/a/atlaspmb/archive/custom'
if not os.path.exists(archive_dir):
    print 'ERROR: '+archive_dir+' is not available'
    sys.exit(1)

nightlies_dir_cvmfs = '/cvmfs/atlas-nightlies.cern.ch/repo/sw'

tmpdir_base = ''
free_space_checks_gb = ''

print 'HOSTNAME: ',os.getenv('HOSTNAME')

if os.getenv('HOSTNAME')=='aibuild001.cern.ch':
    tmpdir_base      = '/build/%s/custom_nightly_tests/rundirs'%username
    free_space_checks_gb=[('/build1',30.0)]#todo: also check quota of target archive dir on afs
else:
    print "job must run on aibuild001.cern.ch"
    sys.exit(0)

##############################################################################
############################# DEFINE JOBS ####################################
##############################################################################

#
#A job function should return None if it doesn't want to run.
#
#Otherwise it should return (infiles,cmd) where infiles is a list of
#  tuple pairs. The former being the input file location on afs or
#  castor and the latter being the local expected location of the input file (should start with tmp_infiles_base).
#  (todo: CASTOR not supported yet)
#
#It is assumed that all output files starts with "my*"
#


def __nevts(nevts,builddate):
    if nevts=='all':
        return -1
    return nevts

####################################################################################################

def Reco_tf_cmd():
    return 'Reco_tf.py'

def __Reco_tf_hitstoaod(infile,nevts,short,amp):
    if short: nevts=1
    f=infile
    preExec=' --conditionsTag \'default:OFLCOND-RUN12-SDR-31\' --conditionsTag \'default:OFLCOND-RUN12-SDR-31\' --digiSeedOffset1 170 --digiSeedOffset2 170 --geometryVersion \'default:ATLAS-R2-2015-03-01-00\' --inputHighPtMinbiasHitsFile /build1/atlaspmb/mc/mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e3581_s2578_s2195/HITS.05608152.*.pool.root.? --inputLowPtMinbiasHitsFile /build2/atlaspmb/mc/mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e3581_s2578_s2195/HITS.05608147.*.pool.root.?  --jobNumber 1  --numberOfCavernBkg="0" --numberOfHighPtMinBias \'0.122680569785\' --numberOfLowPtMinBias \'39.8773194302\'  --outputAODFile="myAOD.pool.root" --outputESDFile="myESD.pool.root" --outputRDOFile="myRDO.pool.root"  --pileupFinalBunch \'6\' --postExec \'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' \'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools["MergeMcEventCollTool"].OnlySaveSignalTruth=True;job.StandardPileUpToolsAlg.PileUpTools["MdtDigitizationTool"].LastXing=150\' \'RDOtoRDOTrigger:from AthenaCommon.AlgSequence import AlgSequence;AlgSequence().LVL1TGCTrigger.TILEMU=True;from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False\' \'ESDtoAOD:CILMergeAOD.removeItem("xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.LATERAL.LONGITUDINAL.SECOND_R.SECOND_LAMBDA.CENTER_MAG.CENTER_LAMBDA.FIRST_ENG_DENS.ENG_FRAC_MAX.ISOLATION.ENG_BAD_CELLS.N_BAD_CELLS.BADLARQ_FRAC.ENG_BAD_HV_CELLS.N_BAD_HV_CELLS.ENG_POS.SIGNIFICANCE.CELL_SIGNIFICANCE.CELL_SIG_SAMPLING.AVG_LAR_Q.AVG_TILE_Q.EM_PROBABILITY.PTD.BadChannelList");CILMergeAOD.add("xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.N_BAD_CELLS.ENG_BAD_CELLS.BADLARQ_FRAC.AVG_TILE_Q.AVG_LAR_Q.CENTER_MAG.ENG_POS.CENTER_LAMBDA.SECOND_LAMBDA.SECOND_R.ISOLATION.EM_PROBABILITY");StreamAOD.ItemList=CILMergeAOD()\' --postInclude \'default:RecJobTransforms/UseFrontier.py\' --preExec \'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(40.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(40);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True);\' \'HITtoRDO:userRunLumiOverride={"run":222525, "startmu":40.0, "endmu":41.0, "stepmu":1.0, "startlb":1, "timestamp": 1376703331}\' \'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.cutLevel.set_Value_and_Lock(14);\' \'ESDtoAOD:ToolSvc += CfgMgr.xAODMaker__TrackParticleCompressorTool( "xAODTrackParticleCompressorTool", OffDiagCovMatrixBits = 7 ); from JetRec import JetRecUtils;f=lambda s:["xAOD::JetContainer#AntiKt4%sJets"%(s,),"xAOD::JetAuxContainer#AntiKt4%sJetsAux."%(s,),"xAOD::EventShape#Kt4%sEventShape"%(s,),"xAOD::EventShapeAuxInfo#Kt4%sEventShapeAux."%(s,),"xAOD::EventShape#Kt4%sOriginEventShape"%(s,),"xAOD::EventShapeAuxInfo#Kt4%sOriginEventShapeAux."%(s,)]; JetRecUtils.retrieveAODList = lambda : f("EMPFlow")+f("LCTopo")+f("EMTopo")+["xAOD::EventShape#NeutralParticleFlowIsoCentralEventShape","xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoCentralEventShapeAux.", "xAOD::EventShape#NeutralParticleFlowIsoForwardEventShape","xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoForwardEventShapeAux.","xAOD::EventShape#ParticleFlowIsoCentralEventShape","xAOD::EventShapeAuxInfo#ParticleFlowIsoCentralEventShapeAux.", "xAOD::EventShape#ParticleFlowIsoForwardEventShape","xAOD::EventShapeAuxInfo#ParticleFlowIsoForwardEventShapeAux.", "xAOD::EventShape#TopoClusterIsoCentralEventShape","xAOD::EventShapeAuxInfo#TopoClusterIsoCentralEventShapeAux.", "xAOD::EventShape#TopoClusterIsoForwardEventShape","xAOD::EventShapeAuxInfo#TopoClusterIsoForwardEventShapeAux.","xAOD::CaloClusterContainer#EMOriginTopoClusters","xAOD::ShallowAuxContainer#EMOriginTopoClustersAux.","xAOD::CaloClusterContainer#LCOriginTopoClusters","xAOD::ShallowAuxContainer#LCOriginTopoClustersAux."]; from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.btaggingAODList=["xAOD::BTaggingContainer#BTagging_AntiKt4EMTopo","xAOD::BTaggingAuxContainer#BTagging_AntiKt4EMTopoAux.","xAOD::BTagVertexContainer#BTagging_AntiKt4EMTopoJFVtx","xAOD::BTagVertexAuxContainer#BTagging_AntiKt4EMTopoJFVtxAux.","xAOD::VertexContainer#BTagging_AntiKt4EMTopoSecVtx","xAOD::VertexAuxContainer#BTagging_AntiKt4EMTopoSecVtxAux.-vxTrackAtVertex"]; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(True);  AODFlags.ThinNegativeEnergyCaloClusters.set_Value_and_Lock(True); AODFlags.ThinNegativeEnergyNeutralPFOs.set_Value_and_Lock(True); from eflowRec.eflowRecFlags import jobproperties; jobproperties.eflowRecFlags.useAODReductionClusterMomentList.set_Value_and_Lock(True); from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");\'  --numberOfCavernBkg \'0\' --preInclude \'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py\'  --mts ESD:0 --steering "RAWtoESD:in-RDO,in+RDO_TRIG,in-BS" --ignoreErrors \'True\' '+amp+' '
    ### MT hook
    ##if 'threads' in amp:
    ##    preExec.split('--postExec')
    return (infile,f),'%s --inputHITSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,preExec)

def __Reco_tf_hitstoaod_q221(infile,nevts,short,amp): 
    if short: nevts=1
    f=infile
    preExec='--conditionsTag \'all:OFLCOND-MC16-SDR-17\' --digiSeedOffset1 170 --digiSeedOffset2 170 --geometryVersion \'default:ATLAS-R2-2015-03-01-00\' --inputHighPtMinbiasHitsFile /build1/atlaspmb/mc/mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e3581_s2578_s2195/HITS.05608152.*.pool.root.? --inputLowPtMinbiasHitsFile /build2/atlaspmb/mc/mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e3581_s2578_s2195/HITS.05608147.*.pool.root.? --jobNumber 1 --numberOfCavernBkg="0" --numberOfHighPtMinBias \'0.122680569785\' --numberOfLowPtMinBias \'39.8773194302\' --pileupFinalBunch \'6\' --preExec  \'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(0.);from LArROD.LArRODFlags import larRODFlags;larRODFlags.nSamples.set_Value_and_Lock(4);from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet="AODFULL"\'  \'HITtoRDO:from Digitization.DigitizationFlags import digitizationFlags;digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];userRunLumiOverride={"run":222525, "startmu":40.0, "endmu":41.0, "stepmu":1.0, "startlb":1, "timestamp": 1376703331}\' \'RAWtoESD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.triggerMenuSetup="MC_pp_v7";from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False;\'  --steering \'doRDO_TRIG\' --DataRunNumber \'222525\' --outputAODFile "myAOD.pool.root" --outputESDFile "myESD.pool.root" --outputRDOFile "myRDO.pool.root" --outputHISTFile "myHIST.root" '
    return (infile,f),'%s --inputHITSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,preExec)

def __Reco_tf_hitstoaod_mc16(infile,nevts,short,isLargeRTrack):
    if short: nevts=1
    f=infile
    preExec='"all:rec.doTrigger.set_Value_and_Lock(False);rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(40.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(40);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)"'
    if isLargeRTrack:
        preExec = preExec + ' "RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doR3LargeD0.set_Value_and_Lock(True);"'
    config=' --postExec "all:CfgMgr.MessageSvc().setError+=[\\"HepMcParticleLink\\"]" "ESDtoAOD:fixedAttrib=[s if \\"CONTAINER_SPLITLEVEL = \'99\'\\" not in s else \\"\\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib" --postInclude "default:PyJobTransforms/UseFrontier.py" --preExec {} --preInclude "HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_run310000.py" "RAWtoESD:LumiBlockComps/LumiBlockMuWriter_jobOptions.py" --skipEvents="0" --autoConfiguration="everything" --valid="True" --conditionsTag "default:OFLCOND-MC16-SDR-25" --geometryVersion="default:ATLAS-R2-2016-01-00-01" --runNumber="410000" --digiSeedOffset1="568" --digiSeedOffset2="568" --digiSteeringConf="StandardSignalOnlyTruth" --AMITag="r10724" --inputHighPtMinbiasHitsFile="/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10701335._*.pool.root.?" --inputLowPtMinbiasHitsFile="/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10701323._*.pool.root.?" --numberOfCavernBkg="0" --numberOfHighPtMinBias="0.2595392" --numberOfLowPtMinBias="99.2404608" --pileupFinalBunch="6" --outputAODFile="myAOD.pool.root" --jobNumber="568" '.format(preExec)
    return (infile,f),'%s --inputHITSFile="%s" --maxEvents="%i" %s'%(Reco_tf_cmd(),f,nevts,config)

def __Reco_tf_rawtoall_mc_itk(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg='--outputRDOFile myRDO.pool.root --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --outputDAOD_IDTRKVALIDFile myDAOD_IDTRKVALID.pool.root --digiSteeringConf StandardInTimeOnlyTruth --geometryVersion ATLAS-P2-ITK-17-06-00 --conditionsTag OFLCOND-MC15c-SDR-14-03 --DataRunNumber 242000 --steering doRAWtoALL --postInclude all:\'InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py\' HITtoRDO:\'InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py\' RAWtoALL:\'InDetSLHC_Example/postInclude.DigitalClustering.py\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4"); from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True); SLHC_Flags.LayoutOption="InclinedAlternative"\' HITtoRDO:\'from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];\' RAWtoALL:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doStandardPlots.set_Value_and_Lock(True);from PixelConditionsServices.PixelConditionsServicesConf import PixelCalibSvc;ServiceMgr +=PixelCalibSvc();InDetFlags.useDCS.set_Value_and_Lock(True);ServiceMgr.PixelCalibSvc.DisableDB=True;from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags;InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True);InDetDxAODFlags.ThinHitsOnTrack.set_Value_and_Lock(False)\' ESDtoDPD:\'rec.DPDMakerScripts.set_Value_and_Lock(["PrimaryDPDMaker/PrimaryDPDMaker.py"]);from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.useDCS.set_Value_and_Lock(True);from PixelConditionsServices.PixelConditionsServicesConf import PixelCalibSvc;ServiceMgr +=PixelCalibSvc();ServiceMgr.PixelCalibSvc.DisableDB=True\' --preInclude all:\'InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu0.py\' HITtoRDO:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py\' default:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py\' RDOMergeAthenaMP:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py\' POOLMergeAthenaMPAOD0:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py,InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py\' POOLMergeAthenaMPDAODIDTRKVALID0:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py,InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py\' --postExec HITtoRDO:\'pixeldigi.EnableSpecialPixels=False; CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' RAWtoALL:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True; from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetTrackSummaryTool.OutputLevel=INFO;from InDetPhysValMonitoring.InDetPhysValMonitoringConf import InDetPhysValDecoratorAlg;decorators = InDetPhysValDecoratorAlg();topSequence += decorators\''
    return (infile,f),'%s --inputHITSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __Reco_tf_mc_phase2_upgrade_mu60(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg='--inputLowPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/user.ncalace.Step3p1.minbias_inelastic_low_EXT0/*.HITS.pool.root --inputHighPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/user.ncalace.Step3p1.minbias_inelastic_high_half_EXT0/*.HITS.pool.root --outputESDFile=myESD.pool.root --jobNumber=1 --digiSteeringConf \'StandardInTimeOnlyTruth\' --conditionsTag \'default:OFLCOND-MC15c-SDR-14-04\' --geometryVersion  ATLAS-P2-ITK-22-00-00 --DataRunNumber 242006 --numberOfHighPtMinBias 0.241724 --numberOfLowPtMinBias 69.7564 --preInclude all:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu60.py\' default:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py\' HITtoRDO:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrains2012Config1_DigitConfig.py,RunDependentSimData/configLumi_muRange.py\' --postInclude all:\'PyJobTransforms/UseFrontier.py,InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/postInclude.SLHC_Setup.py\' HITtoRDO:\'InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py\' RAWtoESD:\'InDetSLHC_Example/postInclude.DigitalClustering.py\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4"); userRunLumiOverride={"run":242006, "startmu":50.0, "endmu":70.0, "stepmu":1.0, "startlb":1, "timestamp":1412006000};from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True);SLHC_Flags.LayoutOption="InclinedAlternative"\' HITtoRDO:\'from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.doBichselSimulation.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];\' RAWtoESD:\'from InDetRecExample.InDetJobProperties import InDetFlags; from PixelConditionsServices.PixelConditionsServicesConf import PixelCalibSvc;ServiceMgr +=PixelCalibSvc();InDetFlags.useDCS.set_Value_and_Lock(True);ServiceMgr.PixelCalibSvc.DisableDB=True;from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags;InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True);InDetDxAODFlags.ThinHitsOnTrack.set_Value_and_Lock(False)\' --postExec HITtoRDO:\'pixeldigi.EnableSpecialPixels=False; CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' RAWtoESD:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True; from AthenaCommon.AppMgr import ToolSvc; svcMgr.AuditorSvc.Auditors.remove("FPEAuditor/FPEAuditor");\''
    return (infile,f),'%s --inputHITSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __Reco_tf_mc_phase2_upgrade_mu200(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg='--inputLowPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/user.ncalace.Step3p1.minbias_inelastic_low_EXT0/*.HITS.pool.root --inputHighPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/user.ncalace.Step3p1.minbias_inelastic_high_half_EXT0/*.HITS.pool.root --outputESDFile=myESD.pool.root --jobNumber=1 --digiSteeringConf \'StandardInTimeOnlyTruth\' --conditionsTag \'default:OFLCOND-MC15c-SDR-14-04\' --geometryVersion  ATLAS-P2-ITK-22-00-00 --DataRunNumber 242020 --numberOfHighPtMinBias 0.725172 --numberOfLowPtMinBias 209.2692 --preInclude all:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu200.py\' default:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py\' HITtoRDO:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrains2012Config1_DigitConfig.py,RunDependentSimData/configLumi_muRange.py\' --postInclude all:\'PyJobTransforms/UseFrontier.py,InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/postInclude.SLHC_Setup.py\' HITtoRDO:\'InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py\' RAWtoESD:\'InDetSLHC_Example/postInclude.DigitalClustering.py\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4"); userRunLumiOverride={"run":242020, "startmu":190.0, "endmu":210.0, "stepmu":1.0, "startlb":1, "timestamp":1412020000};from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True);SLHC_Flags.LayoutOption="InclinedAlternative"\' HITtoRDO:\'from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.doBichselSimulation.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];\' RAWtoESD:\'from InDetRecExample.InDetJobProperties import InDetFlags; from PixelConditionsServices.PixelConditionsServicesConf import PixelCalibSvc;ServiceMgr +=PixelCalibSvc();InDetFlags.useDCS.set_Value_and_Lock(True);ServiceMgr.PixelCalibSvc.DisableDB=True;from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags;InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True);InDetDxAODFlags.ThinHitsOnTrack.set_Value_and_Lock(False)\' --postExec HITtoRDO:\'pixeldigi.EnableSpecialPixels=False; CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' RAWtoESD:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True; from AthenaCommon.AppMgr import ToolSvc; svcMgr.AuditorSvc.Auditors.remove("FPEAuditor/FPEAuditor");\''
    return (infile,f),'%s --inputHITSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __hitstoaod_mc_ttbar(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/mc15_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e3698_s2726/HITS.06950958._003212.pool.root.1'
    return __Reco_tf_hitstoaod(infile,__nevts(nevts,builddate),short,'')

def __hitstoaod_mc_ttbar_mp4(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/mc15_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e3698_s2726/HITS.06950958._003212.pool.root.1'
    return __Reco_tf_hitstoaod(infile,__nevts(nevts,builddate),short,'--athenaopts \' --nprocs=4\'')

def __hitstoaod_mc_ttbar_mt1(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/mc15_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e3698_s2726/HITS.06950958._003212.pool.root.1'
    return __Reco_tf_hitstoaod(infile,__nevts(nevts,builddate),short,'--athenaopts \' --threads=1\'')

def __hitstoaod_mc16_ttbar(branch,cmtcfg,builddate,short,nevts,isLargeRTrack=False):
    infile='/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10504490._000425.pool.root.1'
    return __Reco_tf_hitstoaod_mc16(infile,__nevts(nevts,builddate),short,isLargeRTrack)

def __rawtoall_mc_itk(branch,cmtcfg,builddate,short,nevts):
    infile='/build2/atlaspmb/SLHC_jobs/SIM_100/myHITS.pool.root'
    return __Reco_tf_rawtoall_mc_itk(infile,__nevts(nevts,builddate),short)

def __hitstoesd_mc_phase2_upgrade_mu60(branch,cmtcfg,builddate,short,nevts):
    infile='/build4/atlaspmb/phase2-upgrade-tests/user.ncalace.Step3p1.ttbar._EXT0/*.HITS.pool.root'
    return __Reco_tf_mc_phase2_upgrade_mu60(infile,__nevts(nevts,builddate),short)

def __hitstoesd_mc_phase2_upgrade_mu200(branch,cmtcfg,builddate,short,nevts):
    infile='/build4/atlaspmb/phase2-upgrade-tests/user.ncalace.Step3p1.ttbar._EXT0/*.HITS.pool.root'
    return __Reco_tf_mc_phase2_upgrade_mu200(infile,__nevts(nevts,builddate),short)

# default
def fullchain_mc15_ttbar_valid_13tev_25ns_mu40(**kw): return __hitstoaod_mc_ttbar(nevts=100,**kw)
def fullchain_mc15_ttbar_valid_13tev_25ns_mu40_mp4(**kw): return __hitstoaod_mc_ttbar_mp4(nevts=100,**kw)
def fullchain_mc15_ttbar_valid_13tev_25ns_mu40_mt1(**kw): return __hitstoaod_mc_ttbar_mt1(nevts=100,**kw)
def fullchain_mc16_ttbar_valid_13tev_25ns_mu40(**kw): return __hitstoaod_mc16_ttbar(nevts=100,**kw)
def fullchain_mc16_ttbar_valid_13tev_25ns_mu40_largeRTrack(**kw): return __hitstoaod_mc16_ttbar(nevts=100,isLargeRTrack=True,**kw)
def rawtoall_mc_itk(**kw): return __rawtoall_mc_itk(nevts=100,**kw)
def digireco_mc_phase2_upgrade_mu60(**kw): return __hitstoesd_mc_phase2_upgrade_mu60(nevts=100,**kw)
def digireco_mc_phase2_upgrade_mu200(**kw): return __hitstoesd_mc_phase2_upgrade_mu200(nevts=50,**kw)

#### system

def __Reco_tf_rdotoaod_system(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doTrigger=False;rec.doForwardDet=False;rec.doInDet=True;rec.doMuon=True;rec.doCalo=True;rec.doEgamma=False;rec.doMuonCombined=False;rec.doJetMissingETTag=False;rec.doTau=False;from RecExConfig.RecAlgsFlags import recAlgs;recAlgs.doMuonSpShower=False;rec.doBTagging=False;recAlgs.doEFlow=False;recAlgs.doEFlowJet=False;recAlgs.doMissingET=False;recAlgs.doMissingETSig=False; from JetRec.JetRecFlags import jetFlags;jetFlags.Enabled=False;\'  --ignoreErrors \'True\' --conditionsTag=OFLCOND-RUN12-SDR-25'
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)

def __Reco_tf_rdotoaod_combined(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doMonitoring=False;rec.doTrigger=False;\' --conditionsTag=OFLCOND-RUN12-SDR-25 --ignoreErrors \'True\' '
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)

def __Reco_tf_rdotoaod_monitoring(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doMonitoring=True;rec.doTrigger=False;\' --conditionsTag=OFLCOND-RUN12-SDR-25 --ignoreErrors \'True\' '
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)


def __Reco_tf_rdotoaod_trigger(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doMonitoring=True;rec.doTrigger=True;\' --conditionsTag=OFLCOND-RUN12-SDR-25 --ignoreErrors \'True\' '
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)


def __rdotoaod_mc_ttbar_system(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_system(infile,__nevts(nevts,builddate),short)

def __rdotoaod_mc_ttbar_combined(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_combined(infile,__nevts(nevts,builddate),short)

def __rdotoaod_mc_ttbar_monitoring(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_monitoring(infile,__nevts(nevts,builddate),short)

def __rdotoaod_mc_ttbar_trigger(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_trigger(infile,__nevts(nevts,builddate),short)

# default
def system_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_system(nevts=100,**kw)
def combined_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_combined(nevts=100,**kw)
def monitoring_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_monitoring(nevts=100,**kw)
def trigger_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_trigger(nevts=100,**kw)

####



def __Reco_tf_rawtoall(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg=' --autoConfiguration \'everything\' --conditionsTag  \'CONDBR2-BLKPA-2016-16\' --geometryVersion  \'ATLAS-R2-2015-04-00-00\'  --outputHISTFile  \'myHIST.pool.root\' --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputDRAW_TAUMUHFile  \'myDRAW_TAUMUH.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --athenaopts=\' --pmon=sdmonfp\' --ignoreErrors \'True\' --steering doRAWtoALL'
    return (infile,f),'%s --inputBSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)


def __Reco_tf_rawtoall_mp4(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg=' --autoConfiguration \'everything\' --conditionsTag  \'CONDBR2-BLKPA-2016-16\' --geometryVersion  \'ATLAS-R2-2015-04-00-00\'  --outputHISTFile  \'myHIST.pool.root\' --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputDRAW_TAUMUHFile  \'myDRAW_TAUMUH.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --athenaopts=\' --pmon=sdmonfp\' --ignoreErrors \'True\' --steering doRAWtoALL  --athenaopts \' --nprocs=4\'  '

    return (infile,f),'%s --inputBSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __rawtoall_data16(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/data16_13TeV/data16_13TeV.00305777.physics_Main.daq.RAW._lb0290._SFO-6._0002.data'
    return __Reco_tf_rawtoall(infile,__nevts(nevts,builddate),short)

def __rawtoall_data16_mp4(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/data16_13TeV/data16_13TeV.00305777.physics_Main.daq.RAW._lb0290._SFO-6._0002.data'
    return __Reco_tf_rawtoall_mp4(infile,__nevts(nevts,builddate),short)

# default
def rawtoall_tier0_reco_data16(**kw): return __rawtoall_data16(nevts=500,**kw)
def rawtoall_tier0_reco_data16_mp4(**kw): return __rawtoall_data16_mp4(nevts=-1,**kw)

### data 17
def __Reco_tf_rawtoall_data17(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg=' --autoConfiguration \'everything\' --conditionsTag  \'CONDBR2-BLKPA-2017-10\' --geometryVersion  \'ATLAS-R2-2016-01-00-01\'  --outputHISTFile  \'myHIST.pool.root\' --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputDRAW_TAUMUHFile  \'myDRAW_TAUMUH.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --athenaopts=\' --pmon=sdmonfp\' --ignoreErrors \'True\' --steering doRAWtoALL'
    return (infile,f),'%s --inputBSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __rawtoall_data17(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/data17_13TeV/data17_13TeV.00326446.physics_Main.daq.RAW._lb0154._SFO-1._0001.data'
    return __Reco_tf_rawtoall_data17(infile,__nevts(nevts,builddate),short)

def rawtoall_tier0_reco_data17(**kw): return __rawtoall_data17(nevts=300,**kw)

### data18
def __Reco_tf_rawtoall_data18(infile,nevts,short,isLargeRTrack):
    if short: nevts=1
    f=infile
    preExec='r2a:\'if "TileJetMonTool/TileJetMonTool" in ToolSvc.getSequence(): ToolSvc.TileJetMonTool.do_1dim_histos=True;\'{}'.format('' if not isLargeRTrack else ' r2a:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doR3LargeD0.set_Value_and_Lock(True);\'')
    cfg=' --autoConfiguration \'everything\' --conditionsTag \'CONDBR2-BLKPA-2018-12\' --geometryVersion \'ATLAS-R2-2016-01-00-01\' --outputAODFile \'myAOD.pool.root\' --outputHISTFile \'myHIST.pool.root\' --outputDRAW_EGZFile \'myDRAW_EGZ.pool.root\' --outputDRAW_RPVLLFile \'myDRAW_RPVLL.pool.root\' --outputDRAW_TAUMUHFile \'myDRAW_TAUMUH.pool.root\' --outputDRAW_ZMUMUFile \'myDRAW_ZMUMU.pool.root\' --outputDESDM_CALJETFile \'myDESDM_CALJET.pool.root\' --outputDESDM_EXOTHIPFile \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_MCPFile \'myDESDM_MCP.pool.root\' --outputDESDM_PHOJETFile \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile \'myDESDM_TILEMU.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --AMITag \'f1002\' --athenaopts=\' --pmon=sdmonfp\' --ignoreErrors \'False\' --steering \'doRAWtoALL\' --preExec {} --postExec r2a:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True); TriggerFlags.AODEDMSet="AODFULL"; rec.doAFP.set_Value_and_Lock(True); DQMonFlags.doAFPMon=True;\''.format(preExec)
    return (infile,f),'%s --inputBSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __rawtoall_data18(branch,cmtcfg,builddate,short,nevts,isLargeRTrack=False):
    infile='/build2/atlaspmb/data18_13TeV/data18_13TeV.00364485.physics_Main.daq.RAW._lb0729._SFO-7._0001.data'
    return __Reco_tf_rawtoall_data18(infile,__nevts(nevts,builddate),short,isLargeRTrack)

def rawtoall_tier0_reco_data18(**kw): return __rawtoall_data18(nevts=100,**kw)
def rawtoall_tier0_reco_data18_largeRTrack(**kw): return __rawtoall_data18(nevts=100,isLargeRTrack=True,**kw)

### q431 w/ threads=1
def __Reco_tf_rawtoall_q431_threads(infile,short,nevts,nthreads):
    if short: nevts=1
    f=infile
    return (infile,f),'%s --AMI q431 --maxEvents %i --athenaopts=\'--threads=%i\' --postExec=\'from RecExConfig.RecFlags import rec; rec.doMonitoring=False\''%(Reco_tf_cmd(),nevts,nthreads)

def __rawtoall_q431_threads(branch,cmtcfg,builddate,short,nevts,nthreads):
    infile=''
    return __Reco_tf_rawtoall_q431_threads(infile,short,nevts,nthreads)

def rawtoall_q431_mt1(**kw): return __rawtoall_q431_threads(nevts=100,nthreads=1,**kw)

### Trigger q221 w/ threads=1
def __Reco_tf_rdotordotrigger_q221_threads(infile,nevts,nthreads):
    f=infile
    return (infile, f),'%s --inputRDOFile %s --AMI q221 --maxEvents %i --athenaopts=\'--threads=%i\' --outputRDO_TRIGFile=myRDOTrigger.pool.root --steering=doRDO_TRIG '%(Reco_tf_cmd(),f,nevts,nthreads)
    #return (infile, f),'%s --inputRDOFile %s --AMI q221 --maxEvents %i --athenaopts=\'--threads=%i\' --outputRDO_TRIGFile=myRDOTrigger.pool.root --steering=doRDO_TRIG --postExec=\'all:svcMgr.AuditorSvc.Auditors.remove("ChronoAuditor/ChronoAuditor");svcMgr.ChronoStatSvc.ChronoPrintOutTable=False;svcMgr.ChronoStatSvc.StatPrintOutTable=False;svcMgr.ChronoStatSvc.PrintUserTime=False;\''%(Reco_tf_cmd(),f,nevts,nthreads)

def __rdotordotrigger_q221_threads(branch,cmtcfg,builddate,short,nevts,nthreads):
    infile='/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TriggerTest/valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.RDO.e4993_s3214_r11315/RDO.17533168._000001.pool.root.1'
    return __Reco_tf_rdotordotrigger_q221_threads(infile,nevts,nthreads)

def rdotordotrigger_q221_mt1(**kw): return __rdotordotrigger_q221_threads(nevts=100,nthreads=1,**kw)

### Sim_tf
def Sim_tf_cmd():
    return 'Sim_tf.py'

def __Sim_tf_evnttohits_ttbar(infile,nevts,short,isFast=False):
    if short: nevts=1
    f=infile
    if isFast:
        cfg=' --AMIConfig a894 --simulator ATLFASTII --outputHITSFile myHITS.pool.root '
    else:
        cfg=' --AMIConfig s3505 --outputHITSFile myHITS.pool.root '
    return (infile,f),'%s --inputEVNTFile %s --maxEvents %i %s'%(Sim_tf_cmd(),f,nevts,cfg)

def __Sim_tf_evnttohits_itk(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg='--outputHITSFile myHITS.pool.root --skipEvents 0 --randomSeed 873254 --geometryVersion ATLAS-P2-ITK-17-06-00_VALIDATION --conditionsTag OFLCOND-MC15c-SDR-14-03 --truthStrategy MC15aPlus --DataRunNumber 242000 --preInclude all:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py\' --preExec all:\'from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True); SLHC_Flags.LayoutOption="InclinedAlternative"\' --postInclude all:\'PyJobTransforms/UseFrontier.py,InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/postInclude.SLHC_Setup.py,InDetSLHC_Example/postInclude.SiHitAnalysis.py\' --postExec EVNTtoHITS:\'ServiceMgr.DetDescrCnvSvc.DoInitNeighbours=False; from AthenaCommon import CfgGetter; CfgGetter.getService("ISF_MC15aPlusTruthService").BeamPipeTruthStrategies+=["ISF_MCTruthStrategyGroupIDHadInt_MC15"];\''
    return (infile,f),'%s --inputEVNTFile %s --maxEvents %i %s'%(Sim_tf_cmd(),f,nevts,cfg)

def __evnttohits_ttbar(branch,cmtcfg,builddate,short,nevts,isFast):
    infile='/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/ttbar_muplusjets-pythia6-7000.evgen.pool.root'
    return __Sim_tf_evnttohits_ttbar(infile,__nevts(nevts,builddate),short,isFast)

def __evnttohits_itk(branch,cmtcfg,builddate,short,nevts):
    infile='/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/inputs/EVNT.01485091._001049.pool.root.1'
    return __Sim_tf_evnttohits_itk(infile,__nevts(nevts,builddate),short)

def simulation_ttbar(**kw): return __evnttohits_ttbar(nevts=100,isFast=False,**kw)
def fast_simulation_ttbar(**kw): return __evnttohits_ttbar(nevts=100,isFast=True,**kw)
def simulation_itk(**kw): return __evnttohits_itk(nevts=25,**kw)

## Generate_tf
def Generate_tf_cmd():
    return 'Generate_tf.py'

def __Generate_tf_evgen_minbias_inelastic(infile,nevts,short):
    if short: nevts=1
    f=infile
    jopt = '/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/DSID361xxx/MC15.361033.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic.py'
    cfg  = ' --ecmEnergy=13000 --runNumber=361033 --jobConfig=%s --outputEVNTFile=myEVNT.pool.root '%(jopt)
    return (infile,f),'%s --maxEvents=%i %s'%(Generate_tf_cmd(),nevts,cfg)

def __evgen_minbias_inelastic(branch,cmtcfg,builddate,short,nevts):
    infile=''
    return __Generate_tf_evgen_minbias_inelastic(infile,__nevts(nevts,builddate),short)

def generation_minbias_inelastic(**kw): return __evgen_minbias_inelastic(nevts=1000,**kw)

## Derivations
# mc
def __Reco_tf_aodtodaod_mc(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg = ' --outputDAODFile my.pool.root --reductionConf PHYSVAL --preExec \'rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = "BTagCalibRUN12-08-49"\''
    return (infile,f),'%s --inputAODFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __aodtodaod_mc(branch,cmtcfg,builddate,short,nevts):
    infile='/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/DerivationFrameworkART/AOD.14795494._005958.pool.root.1'
    return __Reco_tf_aodtodaod_mc(infile,__nevts(nevts,builddate),short)

def derivation_physval_mc16(**kw): return __aodtodaod_mc(nevts=1000,**kw)

# data
def __Reco_tf_aodtodaod_data(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg = ' --outputDAODFile my.pool.root --reductionConf PHYSVAL --preExec \'rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = "BTagCalibRUN12Onl-08-49"\''
    return (infile,f),'%s --inputAODFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __aodtodaod_data(branch,cmtcfg,builddate,short,nevts):
    infile='/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/DerivationFrameworkART/data18_13TeV.00348403.physics_Main.merge.AOD.f920_m1947._lb0829._0001.1'
    return __Reco_tf_aodtodaod_data(infile,__nevts(nevts,builddate),short)

def derivation_physval_data18(**kw): return __aodtodaod_data(nevts=500,**kw)

## Overlay jobs

def overlay_mc(**kw):
    inhits = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/OverlayMonitoringRTT/mc16_13TeV.424000.ParticleGun_single_mu_Pt100.simul.HITS.e3580_s3126/HITS.11330296._000376.pool.root.1'
    inbkg  = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/OverlayMonitoringRTT/PileupPremixing/22.0/v4/RDO.merged-pileup.100events.pool.root' # master v4
    amitag = 'd1498'
    maxevt = '100'
    return (inhits,inhits),'{} --AMIConfig {} --inputHITSFile {} --inputRDO_BKGFile {}  --maxEvents {} --outputRDOFile myRDO.pool.root --imf False --athenaopts="--pmon=sdmonfp"'.format(Reco_tf_cmd(), amitag, inhits, inbkg, maxevt)


##########################################################################################################################################################

joblist_default = [
    rawtoall_tier0_reco_data16,
    rawtoall_tier0_reco_data17,
    fullchain_mc15_ttbar_valid_13tev_25ns_mu40,
    simulation_ttbar,
    fast_simulation_ttbar,
    rawtoall_tier0_reco_data18,
    fullchain_mc16_ttbar_valid_13tev_25ns_mu40,
#    generation_minbias_inelastic,
    ]

joblist_derivations = [
    derivation_physval_mc16,
    derivation_physval_data18,
    ]

joblist_multithread = [
#    rawtoall_q431_mt1,
    rdotordotrigger_q221_mt1,
]

joblist_itk = [
    simulation_itk,
    rawtoall_mc_itk,
    digireco_mc_phase2_upgrade_mu60,
    digireco_mc_phase2_upgrade_mu200,
]

joblist_overlay = [
    overlay_mc,
]

joblist_largeRTrack = [
    rawtoall_tier0_reco_data18_largeRTrack,
    fullchain_mc16_ttbar_valid_13tev_25ns_mu40_largeRTrack,
]

#############################################################################

runlist=[]

if os.getenv('HOSTNAME')=='aibuild001.cern.ch':
    runlist+=[('master','x86_64-centos7-gcc8-opt',joblist_default)]
    #runlist+=[('master','x86_64-centos7-clang9-opt',joblist_default)]
    runlist+=[('21.0','x86_64-slc6-gcc62-opt',joblist_default)]
    runlist+=[('21.2','x86_64-slc6-gcc62-opt',joblist_derivations)]
    runlist+=[('master','x86_64-centos7-gcc8-opt',joblist_multithread)]
    runlist+=[('21.9','x86_64-slc6-gcc62-opt',joblist_itk)]
#    runlist+=[('master','x86_64-centos7-gcc8-opt',joblist_overlay)]
    runlist+=[('master','x86_64-centos7-gcc8-opt',joblist_largeRTrack)]
    
#############################################################################


def get_load():
    #duplicated in Misc.py
    p=subprocess.Popen('/bin/ps -eo pcpu --no-headers'.split(), stdout=subprocess.PIPE).communicate()[0]
    return 0.01*sum(map(lambda x:float(x),p.split()))


def get_n_cores():
    #duplicated in Misc.py
    n=0
    for l in open('/proc/cpuinfo'):
        if l.startswith('processor') and l.split()[0:2]==['processor',':']:
            n+=1
    return n


def ok_to_run():
    l=get_load()
    n=get_n_cores()
    if n==1: return l<0.05
    return l<n*0.33


def memory_status(username):
    used_memory = subprocess.Popen("ps -u %s -o rss | awk '{sum+=$1} END {print sum}'" % username,shell=True,stdout=subprocess.PIPE).communicate()[0]                           
    return int(used_memory)


if not ok_to_run():
    sys.exit(0)#not an error, will relaunch later


if memory_status('atlaspmb') > 30000000:
    print 'not enough memory available. exit!'
    sys.exit(0)


##############################################################################
######################### ABORT IF LIMITED SPACE #############################
##############################################################################


def get_free_space(mountpoint):
    if not os.path.exists(mountpoint):
        return None
    for l in subprocess.Popen(['df','-k',mountpoint], stdout=subprocess.PIPE).communicate()[0].split('\n'):
        l=l.split()
        if l and l[-1]==mountpoint: return float(l[-3])/(1024.0*1024.0)

space_ok=True
for mountpoint,limit in free_space_checks_gb:
    fs=get_free_space(mountpoint)
    if fs==None:
        print "Could not determine space left on %s"%mountpoint
        space_ok=False
    elif fs<limit:
        print "Too little space left on %s : %.2f gb (required %.2f gb)"%(mountpoint,fs,limit)
        space_ok=False
if not space_ok:
    print "Aborting."
    sys.exit(1)


##############################################################################
#################### FIGURE OUT WHICH JOBS TO RUN ############################
##############################################################################


def touch(filename):
    #only creates if doesn't exist. Won't update timestamp of existing file.
    if not os.path.exists(filename):
        open(filename,'wa').close()

def decode_build_date(relNstr,modtime):
    assert relNstr.startswith('rel_') and len(relNstr)==5 and relNstr[-1] in '0123456'
    weekday=int(relNstr[4:])
    weekday=(weekday-1)%7#to get monday=0, sunday=6
    assert weekday>=0 and weekday<=6
    builddate=datetime.date.fromtimestamp(modtime)
    #Adjust builddate so it matches the rel_X number exactly (rel_1=monday, etc.):
    if builddate.weekday()!=weekday:
        #print "WARNING: Correcting date!!!"
        #print weekday,'  ',builddate.weekday()
        deltadays=weekday-builddate.weekday()
        if deltadays>=5: deltadays-=7
        if deltadays<=-5: deltadays+=7
        #print deltadays
        #assert deltadays>=-2 and deltadays<=2
        builddate=builddate+datetime.timedelta(deltadays)
    return builddate

for b,cmtcfg,joblist in runlist:

    #Define the ATLAS project according to the release
    atlas_project = 'Athena'
    if b == '21.2':
        atlas_project = 'AthDerivation'

    #pattern='%s/%s/*/%s/*/InstallArea/%s/bin'%(nightlies_dir_cvmfs,b,atlas_project,cmtcfg)
    pattern='%s/%s_%s_%s/*/%s/*/InstallArea/%s/bin'%(nightlies_dir_cvmfs,b,atlas_project,cmtcfg,atlas_project,cmtcfg)
    
    print b,cmtcfg,joblist
    print pattern
    dirs=sorted(glob.glob(pattern))
    
    for d in dirs:
        print d
        if not os.path.isdir(d): continue#protect against afs issues
        dtime=os.path.getmtime(d)

        hours_ago=(time.time()-dtime)/(3600.0)
        #To stay away from builds in progress the timestamp should have an age of at least 2 hours
        if hours_ago<2:
            print 'hours_ago<2'
            continue
        #To stay away from builds about to be restarted, the timestamp should have an age of at most 5 days.
        if hours_ago>24*6:
            print 'hours_ago>24*6'
            continue
        #Get build date:
        relN=d.split('/')[-6]

        relsplit=relN.split('-')
        if (len(relsplit) != 3): continue
        year=relsplit[0]
        month=relsplit[1]
        day=relsplit[2].split('T')[0]
        builddate=datetime.date(int(year), int(month), int(day))

        build_archive_dir = os.path.join(archive_dir,builddate.strftime('%d/%m/%Y'),b,cmtcfg)
        #Figure out atlas setup command:
        asetup_args=cmtcfg.split('-')+[b,relN]
        if asetup_args[0]=='x86_64': asetup_args[0]='64'
        else: asetup_args[0]='32'
        for ival,val in enumerate(asetup_args):
            if 'T' in val and '-' in val and 'r' not in val:
                asetup_args[ival] = 'r'+asetup_args[ival] # only rYYYY-MM-DDTHHMM works as of 30/01/2019
        asetup_cmd = 'lsetup "asetup '+','.join(asetup_args)+','+atlas_project+'"'  

        #Figure out which jobs to run:
        for job in joblist:

            print job.__name__

            #Get command + short command:
            infiles,cmd=job(branch=b,cmtcfg=cmtcfg,builddate=builddate,short=False) #old
            #infiles,cmd=job
            if not cmd: continue#not on this build apparently
            '''
            for srcfile,localfile in infiles:
                if os.path.exists(localfile): continue
                if not os.path.isdir(os.path.dirname(localfile)):
                    os.makedirs(os.path.dirname(localfile))
                #print srcfile
                #print localfile
                if 'root://eosatlas///' in srcfile:
                    eos_copy_cmd='xrdcp '+srcfile+' '+localfile
                    os.system(eos_copy_cmd)
                else: 
                    shutil.copy2(srcfile,localfile)
                assert os.path.exists(localfile)
            '''
            infiles_short,cmd_short=job(branch=b,cmtcfg=cmtcfg,builddate=builddate,short=True)
            assert infiles==infiles_short
            assert cmd_short
            
            #Check if done already done, and if not acquire "lock" by making target output dir:
            target_dir=os.path.join(build_archive_dir,job.__name__)
            file_done=os.path.join(target_dir,'__done')
            file_start=os.path.join(target_dir,'__start')

            if os.path.isdir(target_dir):
                if os.path.exists(file_done):
                    continue
                #already running or done (todo: check here that the job actually finished in 24 hours... otherwise delete and relaunch)
                if not os.path.exists(file_start):
                    time.sleep(60)
                if not os.path.exists(file_start) or (time.time()-os.path.getmtime(file_start))>24*3600:
                    print "Warning: Having to remove %s"%target_dir
                    shutil.rmtree(target_dir)
                else:
                    print "elseing target dir"
                    continue

            try:
                os.makedirs(target_dir)#Now we are in charge...
                assert os.path.isdir(target_dir)
                touch(file_start)
            except Exception,err:
                print "ERROR: Problems encountered while making %s"%target_dir
                print "Aborting."
                if os.path.isdir(target_dir): shutil.rmtree(target_dir)
                sys.exit(1)
                pass

            #Temporary run-dir:
            tmpdir=os.path.join(tmpdir_base,'__'.join([b,cmtcfg,relN,job.__name__]))
            tmpdir_start=os.path.join(tmpdir,'__start')
            if os.path.exists(tmpdir):
                canremove=False
                if not os.path.exists(tmpdir_start): time.sleep(20)
                if not os.path.exists(tmpdir_start): canremove=True
                elif (time.time()-os.path.getmtime(tmpdir_start))>24*3600: canremove=True
                if not canremove:
                    print "WARNING: Found unexpected recent tmp rundir %s"%tmpdir
                    continue
                shutil.rmtree(tmpdir)
            os.makedirs(tmpdir)
            touch(tmpdir_start)
            #NB: Stop after one job has run! Cron will relaunch us each hour and will then run the next job!
            def cmd_gen(_cmd,_asetup_cmd,rundir,infiles,relN,builddate):
                file_command=os.path.join(rundir,'__command.sh')
                cmds=['#!/bin/bash',
                      '','#Input files are copied locally before running. Sources are:']
                i=0
                '''
                for src,local in infiles:
                    i+=1
                    cmds+=['# %i) %s => %s'%(i,src,local)]
                '''
                cmds+=['source ~/.bashrc;']
                cmds+=['source ~/.bash_profile;']
                cmds+=['export TRF_ECHO=1;']
                cmds+=['','touch __start_asetup',_asetup_cmd]
                if atlas_project == 'AthDerivation': ## Temporary hack 2018/09/18
                    cmds+=['','## Temporary Hack ##','export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/atlas-nightlies.cern.ch/repo/sw/21.2/sw/lcg/releases/gcc/6.2.0/x86_64-slc6/lib64','']
                cmds+=['touch __start',_cmd+' >__log.txt 2>&1','echo $? > __exitcode']

                dayofweek=builddate.weekday()-1
                relN
                cmds+=['perl -pi -e "s/private\/private/${AtlasBuildBranch}\/rel'+str(dayofweek)+'/g;" __log.txt']
                if atlas_project == 'AthDerivation':
                    cmds+=['mv DAOD_PHYSVAL.my.pool.root myDAOD_PHYSVAL.pool.root']
                cmds+=['for f in my*.pool.root; do',
                       '    if [ -f "$f" ]; then checkxAOD.py $f > $f.checkfile.txt 2>/dev/null; fi',
                       'done']
                cmds+=['BAD=0',
                       'for f in ntuple*.pmon.gz; do',
                       '    if [ -f "$f" ]; then tar xf $f "*.pmonsd.*" || BAD=1; fi',
                       'done',
                       'if [ $BAD != 0 ]; then echo "ERROR: tar problems in $PWD"; fi']
                cmds+=["ls -oqAQ1S --block-size=1 --ignore='__*' > __dirlist.txt"]

                cmds+=['rm -f *.pool.root']
                cmds+=['#Python code to create sqlite file']
                ##if 'master' in asetup_args:
                ##    cmds+=['lsetup "lcgenv -p LCG_95 x86_64-slc6-gcc8-opt pytools"'] # This bit is to get the python stuff
                cmds+=['export PYTHONPATH=${BASEDIR}/PerformanceMonitoring/python:$PYTHONPATH']
                cmds+=['python << END']
                cmds+=['']
                cmds+=['import pmbDB as mydb']
                cmds+=['import os']
                cmds+=['x=mydb.pmbDB(os.path.dirname(os.path.realpath(\'__exitcode\')).replace(\'/\',\'__\')+\'.db\')']
                cmds+=['jobname=\''+job.__name__+'\'']
                cmds+=['release=os.environ[\'AtlasBuildBranch\']']
                cmds+=['platform=os.environ[\'%s_PLATFORM\']'%(atlas_project)]
                cmds+=['nightly=\''+relN+'\'']
                cmds+=['x.addEntry(release,platform,nightly,jobname,\'./\')']
                cmds+=['']
                cmds+=['END']
                cmds+=['rsync *.db /build/atlaspmb/custom_nightly_tests/database_staging_area/.']
                cmds+=['gzip *.txt','touch __done','']

                fh=open(file_command,'w')
                fh.write('\n'.join(cmds))
                fh.close()
                return file_command

            tmpdir_shortrun=os.path.join(tmpdir,'prerun')
            os.makedirs(tmpdir_shortrun)
            tmpdir_run=os.path.join(tmpdir,'run')
            os.makedirs(tmpdir_run)
            cmdfile=cmd_gen(cmd,asetup_cmd,tmpdir_run,infiles,relN,builddate)
            ec2=os.system('cd %s && source %s'%(tmpdir_run,cmdfile))
            if ec2:
                print "NB: Problems in job",target_dir
            collect=[]
            for f in os.listdir(tmpdir_run):
                if os.path.isdir(f): continue
                bn=os.path.basename(f)
                if bn.startswith('__') or '.db' in bn or '.checkfile.txt.gz' in bn or 'pmonsd' in bn or 'ntuple' in bn or bn.startswith('log') or bn.startswith('mem') or bn.startswith('prmon') or '.json' in bn:
                    collect+=[bn]
            for c in collect: #Could be susceptible to EOS failures
                shutil.copy2(os.path.join(tmpdir_run,c),target_dir)
            shutil.rmtree(tmpdir)

            touch(file_done)
            sys.exit(0)#On purpose we stop after one job has run...
