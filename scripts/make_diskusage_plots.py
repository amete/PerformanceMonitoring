import sys
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.dates import DateFormatter
plt.style.use('seaborn-whitegrid')
from datetime import timedelta

# Define variables
def getvariables(hostname):
    if hostname == 'uclhc-1.ps.uci.edu':
        node_short  = 'uclhc'
        workdir     = '/home/amete/system_monitoring/'
        folders     = ['home','uci']
    elif hostname == 'atpc002':
        node_short  = 'atpc002'
        workdir     = '/atlas/scratch0/amete/system_monitoring/'
        folders     = ['scratch']
    elif hostname == 'gpatlas2.local':
        node_short  = 'gpatlas2'
        workdir     = '/home/amete/system_monitoring/'
        folders     = ['data7']
    return (workdir,folders,node_short)

# Helper function to annotate
def autolabel(rects,ax):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

# Helper function to color the bars
def autocolor(rects):
    colors=['red','navy','orange','green','darkviolet','orangered','olive','blue','firebrick','teal','purple','coral','steelblue','seagreen']
    for idx,rect in enumerate(rects):
        rect.set_color(colors[idx])

# Helper function to get the timestamp
def gettimestamp(filename):
    with open(filename) as f:
        for line in f:
            if '#' in line:
                timestamp = line[2:]
                return timestamp

# Make disk usage plot
def make_disk_plot(workdir,folder,node_short):
    # Plot Top 10
    N = 10

    # Load the disk 
    filename  = '%s/du-logs/%s-latest.log'%(workdir,folder)
    disk_info = pd.read_csv(filename, sep = '\t', header=None, skiprows=1)
    disk_info = disk_info[0:N] # Pick Top N
    disk_info[1] = disk_info.apply(lambda x: str(x[1]).split('/')[-1], axis = 1)

    # Make a simple bar plot
    indices = np.arange(N)
    width = 0.5
    fig, ax1 = plt.subplots()
    bars = plt.bar(indices + width*0.5, disk_info[0]/1024./1024., width, color='blue')
    plt.title('Disk Usage by User in the %s folder'%(folder.upper()))
    plt.xlabel('User \n Date: %s'%(gettimestamp(filename)))
    plt.ylabel('Usage [GB]')
    plt.xticks(rotation=90)
    ax1.set_xticks(indices + width)
    ax1.set_xticklabels(disk_info[1])
    autolabel(bars,ax1)
    autocolor(bars)
    plt.ylim(ymax=disk_info[0].iloc[0]/1024./1024.*1.15)
    fig.subplots_adjust(bottom=0.2)
    fig.savefig('%s-disk-%s-info.png'%(node_short,folder))

# Make plots
if '__main__' in __name__:
    workdir,folders,node_short = getvariables(sys.argv[1])
    for folder in folders:
        make_disk_plot(workdir,folder,node_short)
